#!/bin/bash

# To run this, simply cd to the Broken folder and run:

# . build/weeklychangelog.sh :) *After syncing the repo.*

_now=$(date +"%m-%d-%Y")
_file=changelog/CMChangelog/$_now/CM-Changelog-$_now.txt

mkdir -p changelog/CMChangelog/

mkdir -p changelog/CMChangelog/$_now

chmod 777 -R changelog/CMChangelog

repo forall -pc git log --oneline --reverse --no-merges --since=7.day.ago >  $_file
